FROM python:alpine3.17

WORKDIR /APP

COPY . /APP 

run python3 -m pip install -r requirements.txt

EXPOSE 5000

CMD ["gunicorn", "--bind", "0.0.0.0:5000", "--workers", "2", "main:app"]



