import flask
from flask import Flask, request
import bs4
import requests
from werkzeug.exceptions import HTTPException

'''



This file is used as a backend for a web application that displays the temperature during
the day and night(celsius), the percentage of humidity in different cities / countries.

Modules used:
FLASK and BEAUTIFULSOUP(bs4)

API:
Weather Query Builder

How to use:
The user should enter the name of a country/city and press the "GET TEMPERATURE" button,
at this point the function "get_data" will be called which checks the correctness of the user's input.
A call to the API will be made and if the response code is 200, 
the information will be converted to a JSON object and analyzed to obtain the relevant information.
At this point the BEAUTIFULSOUP module will build the HTML file and place the relevant information
in the appropriate location in the file.
Finally, the HTML file will be returned with the information according to the user's search.
--------------------------------------------------------------------
                     |                            |                  |                                
       Author        |         CodeReview         |        Date      | 
    Shmuel Hayun     |      Nitsan David Gabay    |      15.12.22    |
                     |                            |                  | 
--------------------------------------------------------------------   
'''

app = Flask(__name__)


@app.get('/')
def home():
    '''
    The function return the landing page at program startup
    - home.html file
    '''
    try:
        return flask.send_file("home.html")
    except HTTPException as e:
        print('Error in home function', e.args)
    except Exception as e:
        print('Error in home function', e.args)


@app.route('/get_image')
def get_image():
    '''
    return the background.jpeg for home.html
    and error/empty_error files
    '''
    try:
        return flask.send_file("background.jpeg")
    except HTTPException as e:
        print('Error in get_image function', e.args)
    except Exception as e:
        print('Error in get_image function', e.args)


@app.route('/get_result_image')
def get_result_image():
    '''
    return the result_image2.jpg for result.html file
    '''
    try:
        return flask.send_file("result_image2.jpg")
    except HTTPException as e:
        print('Error in get_result_image function', e.args)
    except Exception as e:
        print('Error in get_result_image function', e.args)


def is_valid_input(city):
    '''
    check if city / country name is valid
    :param city:
    :return: boolean value
    '''

    res = True
    if city in ('', '/', '    '):
        res = False
    for char in city:
        if char.isdigit():
            res = False
    return res


@app.post('/get_data')
def get_data():
    '''
    The function checks the response code after the call to API.
    Analyzes the JSON object to obtain the relevant information
    and processes the information.
    returns the HTML file with the relevant information.
    '''
    try:

        city = request.form.get("textFieldId")
        if not is_valid_input(city):
            return flask.send_file("error.html")
        api_key = "MZL76DBA3SCZUQ78U9L7FXMBB"
        link = f"https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/{city}?unitGroup=us&key={api_key}&contentType=json"
        response = requests.get(link)
        if response.status_code != 200:
            print(response.text)
            return flask.send_file("error.html")
        json_obj = response.json()
        soup = bs4.BeautifulSoup('<html><body></body></html>', features="html.parser")
        body = soup.find('body')
        body.append(soup.new_tag('style', type='text/css'))
        body.style.append('body {background-image: url("http://127.0.0.1:5000/get_result_image");background-size: 1800px;width: 1200px;height: 600px;}')
        country_city = f'<p style="color:brown;background-color: none;direction: rtl;text-align: center;font-size: 25px;">{json_obj["resolvedAddress"]}</p>'
        country_city_snippet = bs4.BeautifulSoup(country_city, features="html.parser").p.extract()
        soup.body.append(country_city_snippet)
        for i in range(7):
            date_time = f'<p style="color:blue;background-color: none;direction: rtl;text-align: center;">תאריך : {json_obj["days"][i]["datetime"]}</p>'
            snippet = bs4.BeautifulSoup(date_time, features="html.parser").p.extract()
            soup.body.append(snippet)
            day_humidity = f'<p style="color:black;background-color:none;direction: rtl;text-align: center;">לחות : {json_obj["days"][i]["humidity"]}%</p>'
            snippet_humidity = bs4.BeautifulSoup(day_humidity, features="html.parser").p.extract()
            soup.body.append(snippet_humidity)
            # handle night
            avg_night = 0
            for day in range(9):
                avg_night += json_obj["days"][i]["hours"][day]["temp"]
            for day in range(21, 24):
                avg_night += json_obj["days"][i]["hours"][day]["temp"]
            avg_night = avg_night / 12
            temp_in_celsius = f'<p style="color:black;background-color: none;direction: rtl;text-align: center;">טמפרטורה ממוצעת בלילה : {int((avg_night-32)*(5/9))}°</p>'
            snippet_temp = bs4.BeautifulSoup(temp_in_celsius, features="html.parser").p.extract()
            soup.body.append(snippet_temp)
            # handle day
            avg_day = 0
            for day in range(9, 21):
                avg_day += json_obj["days"][0]["hours"][day]["temp"]
            avg_day = avg_day / 12
            temp_in_celsius = f'<p style="color:black;background-color: none;direction: rtl;text-align: center;">טמפרטורה ממוצעת ביום : {int((avg_day-32)*(5/9))}°</p>'
            snippet_temp_day = bs4.BeautifulSoup(temp_in_celsius, features="html.parser").p.extract()
            soup.body.append(snippet_temp_day)
        tests = f'<form class="center" action="/get_data" method="POST" id="container"><input name="textFieldId" type="text" placeholder="Enter City Name / Country"><input type="submit" value="Get Temperature"></form>'
        tests_snippet = bs4.BeautifulSoup(tests, features="html.parser").extract()
        soup.body.append(tests_snippet)
        # save the file
        with open("result.html", "w") as outf:
            outf.write(str(soup))
        return flask.send_file("result.html")

    except HTTPException as e:
        print('Error in get_data function', e.args)
    except Exception as e:
        print('Error in get_data function', e.args)


if __name__ == '__main__':
    app.run(host="0.0.0.00")
