import main
import unittest
import requests


class TestAppArguments(unittest.TestCase):
    '''
    class to test the user input

    '''

    def test_empty(self):
        self.assertEqual(False, main.is_valid_input(""))

    def test_digits_input(self):
        self.assertEqual(False, main.is_valid_input("87678678"))

    def test_slash_input(self):
        self.assertEqual(False, main.is_valid_input("/"))

    def test_city_input(self):
        self.assertEqual(True, main.is_valid_input("IsRaEl"))

    def test_tab_input(self):
        self.assertEqual(False, main.is_valid_input("    "))

    # def test_check_API(self):
    #     requests.post("http://127.0.0.1:5000/get_data")


if __name__ == '__main__':
    unittest.main()
